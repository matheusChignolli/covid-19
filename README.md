# COVID-19 Chignolli

Referência para criar projeto TS: https://rapidapi.com/blog/create-react-app-typescript/?utm_source=google&utm_medium=cpc&utm_campaign=DSA&gclid=CjwKCAjwr7X4BRA4EiwAUXjbtwmK2efXaip5j-QGQZGkVh5DELkbOrxjygtv36Adx2o0qVO2YsMyHhoCKqkQAvD_BwE

## Para rodar o projeto basta seguir os passos

* npm install
* npm start ou npm run build

## Tecnologias utilizadas

* create-react-app com TS (command: npx create-reat-app covid-19-chignolli --template typescript)
* axios (command: npm install --save axios)
