export interface StateData {
    uid: number,
    uf: string,
    state: String,
    cases: number,
    deaths: number,
    suspects: number,
    refuses: number,
    datetime: String,
}